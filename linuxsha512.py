import crypt
words = open("common_passwords","r")
hashes = open("/etc/shadow","r")

def checkhash(x):
	salt,hash = (x.split(":")[1]).rsplit("$",1)
	for word in words.readlines():
		word = word.strip("\n")
		if crypt.crypt(word,salt) == (salt+"$"+hash):
			return word
	return "not in the list"

for j in [i.strip("\n") for i in hashes.readlines() if "$" in i]:
	print checkhash(j)
